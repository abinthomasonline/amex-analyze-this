import pandas as pd
import numpy as np

alpha = pd.read_csv("../data/alpha_submission.csv")

alpha = alpha.sort_values(by='p0', ascending=False)


alpha.to_csv("../submissions/Tilt_IITMadras_60.csv", index=False, header=False)