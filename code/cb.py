import pandas as pd
import numpy as np
from catboost import CatBoostClassifier

train = pd.read_csv("../data/c_train.csv")
test = pd.read_csv("../data/c_leaderboard.csv")

X = train.drop(['application_key', 'default_ind'], axis=1)
y = train.default_ind

from sklearn.model_selection import train_test_split
X_train, X_validation, y_train, y_validation = train_test_split(X, y, train_size=0.8, random_state=1234, stratify=y)

print(X.dtypes)

#categorical_features_indices = np.where(X.dtypes != np.float)[0]
categorical_features_indices = np.array(range(0, 47))
print(categorical_features_indices)

#from catboost import CatBoostRegressor
model=CatBoostClassifier(depth=4, learning_rate=0.1, loss_function='Logloss', num_trees=400)
model.fit(X_train, y_train, cat_features=categorical_features_indices, eval_set=(X_validation, y_validation), plot=False)
model.save_model('../models/catboost2', format="cbm")

#model = CatBoostClassifier()
#model.load_model('../models/catboost')

submission = pd.DataFrame()
p = test.drop(['application_key'], axis=1)
k = model.predict_proba(p)
submission = pd.DataFrame(data=k[0:,0:], index=np.array(range(1, 25001)), columns=np.array(['p0', 'p1']))
submission['application_key'] = test.application_key
submission['default_ind'] = model.predict(p)
submission.to_csv("../data/alpha_submission.csv")