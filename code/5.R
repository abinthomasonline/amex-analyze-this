setwd(dir = "/Users/abinthomasonline/Desktop/amex/code")

train <- read.csv(file = '../data/Training_dataset_Original.csv',header = TRUE)
leaderboard <- read.csv(file = '../data/Leaderboard_dataset.csv',header = TRUE)
evaluation <- read.csv(file = '../data/Evaluation_dataset.csv',header = TRUE)

n_train <- nrow(train)
n_leaderboard <- nrow(leaderboard)
n_evaluation <- nrow(evaluation)

dataset <- rbind(train[,1:48], leaderboard, evaluation)

for (i in 2:47) {
  dataset[, i] <- as.numeric(as.character(dataset[, i]))
  print(paste0(i, " : ", sum(is.na(dataset[, i]))))
}
dataset[, 49] <- numeric(n_train+n_leaderboard+n_evaluation)
dataset[dataset[, 48]=='C', 49] <- 1
dataset <- dataset[, c(1:47, 49)]
colnames(dataset)[48] <- 'mvar47'

na_features <- c(2:6, 17:25, 34:40, 44:47)
for (i in na_features) {
  cname <- colnames(dataset)[i]
  dataset[, paste0("na_", cname)] <- numeric(n_train+n_leaderboard+n_evaluation)
  dataset[is.na(dataset[, i]), paste0("na_", cname)] <- 1
}
for (i in 2:48) {
  cname <- colnames(dataset)[i]
  dataset[is.na(dataset[, i]), cname] <- median(dataset[, cname], na.rm = TRUE)
}

train <- cbind(dataset[1:n_train,], train[,49])
colnames(train)[74] <- 'default_ind'
train$default_ind <- factor(x = train$default_ind)
leaderboard <- dataset[(n_train+1):(n_train+n_leaderboard), ]
evaluation <- dataset[(n_train+n_leaderboard+1):(n_train+n_leaderboard+n_evaluation),]

write.csv(x = train, file = '../data/train.csv', row.names = FALSE)
write.csv(x = leaderboard, file = '../data/leaderboard.csv', row.names = FALSE)
write.csv(x = evaluation, file = '../data/evaluation.csv', row.names = FALSE)

library(h2o)
h2o.init()

h2o_train <- as.h2o(train)
splits <- h2o.splitFrame(data = h2o_train, ratios = c(.7, .15), destination_frames = c("train", "test", "valid"))

learn_rate_opt <- c(0.05, 0.1, 0.15, 0.2)
max_depth_opt <- c(4, 6, 8)
sample_rate_opt <- c(0.7, 0.8, 0.9, 1.0)
col_sample_rate_opt <- c(0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8)
hyper_params <- list(learn_rate = learn_rate_opt,
                     max_depth = max_depth_opt,
                     sample_rate = sample_rate_opt,
                     col_sample_rate = col_sample_rate_opt)

search_criteria <- list(strategy = "RandomDiscrete",
                        max_models = 10,
                        seed = 1)

gbm_grid <- h2o.grid(algorithm = "gbm",
                     grid_id = "gbm_grid_binomial",
                     x = c(2:73),
                     y = 74,
                     training_frame = splits[[1]],
                     ntrees = 200,
                     nfolds = 5,
                     fold_assignment = "Modulo",
                     keep_cross_validation_predictions = TRUE,
                     hyper_params = hyper_params,
                     search_criteria = search_criteria)

# Train a stacked ensemble using the GBM grid
ensemble <- h2o.stackedEnsemble(x = c(2:73),
                                y = 74,
                                training_frame = splits[[1]],
                                model_id = "ensemble_gbm_grid_binomial",
                                base_models = gbm_grid@model_ids, validation_frame = splits[[3]])

summary(ensemble)
h2o.performance(ensemble, splits[[2]])

predict <- h2o.predict(ensemble, newdata = as.h2o(leaderboard))
predict <- as.data.frame(predict)
predict <- cbind(leaderboard[, 1], predict)
predict <- predict[order(-predict$p0),]
colnames(predict)[1] <- 'application_key'

# default_ind <- numeric(25000)
# default_ind[predict$p0<0.73] <- 1

#submission <- cbind(predict[, 1], default_ind)

switch_idx <- 10000

submission <- rbind(predict[1:switch_idx, 1:2], predict[(25000):(15000+switch_idx), 1:2], predict[(switch_idx+1):(14999+switch_idx), 1:2])
#colnames(submission)[1] <- 'application_key'

write.table(x = submission, file = '../submissions/Tilt_IITMadras_57.csv', row.names = FALSE, col.names = FALSE, sep = ',')
