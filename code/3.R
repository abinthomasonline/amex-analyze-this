setwd(dir = "/Users/abinthomasonline/Desktop/amex/code")

train <- read.csv(file = '../data/Training_dataset_Original.csv',header = TRUE)
leaderboard <- read.csv(file = '../data/Leaderboard_dataset.csv',header = TRUE)
evaluation <- read.csv(file = '../data/Evaluation_dataset.csv',header = TRUE)

n_train <- nrow(train)
n_leaderboard <- nrow(leaderboard)
n_evaluation <- nrow(evaluation)

dataset <- rbind(train[,1:48], leaderboard, evaluation)

for (i in 2:47) {
  dataset[, i] <- as.numeric(as.character(dataset[, i]))
}
dataset[, 49] <- numeric(n_train+n_leaderboard+n_evaluation)
dataset[dataset[, 48]=='C', 49] <- 1
dataset <- dataset[, c(1:47, 49)]
colnames(dataset)[48] <- 'mvar47'

na_features <- c(2:6, 17:25, 34:40, 44:47)
for (i in na_features) {
  cname <- colnames(dataset)[i]
  dataset[, paste0("na_", cname)] <- numeric(n_train+n_leaderboard+n_evaluation)
  dataset[is.na(dataset[, i]), paste0("na_", cname)] <- 1
  dataset[is.na(dataset[, i]), cname] <- median(dataset[, cname], na.rm = TRUE)
}

train <- cbind(dataset[1:n_train,], train[,49])
colnames(train)[74] <- 'default_ind'
leaderboard <- dataset[(n_train+1):(n_train+n_leaderboard), ]
evaluation <- dataset[(n_train+n_leaderboard+1):(n_train+n_leaderboard+n_evaluation),]

write.csv(x = train, file = '../data/train.csv', row.names = FALSE)
write.csv(x = leaderboard, file = '../data/leaderboard.csv', row.names = FALSE)
write.csv(x = evaluation, file = '../data/evaluation.csv', row.names = FALSE)
